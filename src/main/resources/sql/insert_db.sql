insert into roles
--values (1, 'Роль работника', 'WORKER'),
  --     (2, 'Роль администратора', 'ADMIN');
values (1, 'Роль пользователя', 'USER'),
       (2, 'Роль работника', 'WORKER');



insert into films
values (nextval('films_seq'), 'Австрия', 0, null, 2015, 'Зведная пыль'),
       (nextval('films_seq'), 'Франция', 2,null, 2011, 'Дом у озера'),
       (nextval('films_seq'), 'США', 0 ,null, 2017, 'Глубина'),
       (nextval('films_seq'), 'Россия', 2,null, 2010, 'Дон'),
       (nextval('films_seq'), 'Италия', 3,null, 2010, 'Облака'),
       (nextval('films_seq'), 'Испания', 3,null, 2010, 'Прыжки в небо'),
       (nextval('films_seq'), 'Китай', 0,null, 2010, 'Пустыня'),
       (nextval('films_seq'), 'Корея', 1,null, 2010, 'Луна'),
       (nextval('films_seq'), 'США', 0,null, 2010, 'Пыль');

insert into directors
values (nextval('directors_seq'), 'Ридли Скотт', 'Режиссер'),
       (nextval('directors_seq'),'Майкл Манн', 'Режиссер'),
       (nextval('directors_seq'),'Джеймс Кэмерон', 'Режиссер'),
       (nextval('directors_seq'),'Дэвид Финчер', 'Режиссер'),
       (nextval('directors_seq'),'Сэм Рейми', 'Режиссер'),
       (nextval('directors_seq'),'Гильермо дель Торо', 'Режиссер'),
       (nextval('directors_seq'),'Николай Лебедев', 'Режиссер');

insert into films_directors(film_id, director_id)
values (9,1),
       (8,2),
       (7,3),
       (6,4),
       (5,5),
       (4,6),
       (3,7),
       (2,1),
       (1,2);




