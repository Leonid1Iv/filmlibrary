package ru.edu.filmlibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.Callable;

@SpringBootApplication
public class FilmLibraryApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(FilmLibraryApplication.class, args);

    }
}
