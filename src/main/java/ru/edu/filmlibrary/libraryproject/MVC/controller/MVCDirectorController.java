package ru.edu.filmlibrary.libraryproject.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.edu.filmlibrary.libraryproject.dto.AddFilmDTO;
import ru.edu.filmlibrary.libraryproject.dto.DirectorDTO;
import ru.edu.filmlibrary.libraryproject.dto.DirectorWithFilmsDTO;
import ru.edu.filmlibrary.libraryproject.service.DirectorService;
import ru.edu.filmlibrary.libraryproject.service.FilmService;

import java.util.List;

@Hidden
@Controller
@RequestMapping("directors")
@Slf4j
public class MVCDirectorController {
    private final DirectorService directorService;
    private final FilmService filmService;

    public MVCDirectorController(DirectorService directorService, FilmService filmService) {
        this.directorService = directorService;
        this.filmService = filmService;
    }


    // обновление директора
    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("director", directorService.getOne(id));
        return "directors/updateDirector";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("directorForm") DirectorDTO directorDTO) {
        directorService.update(directorDTO);
        return "redirect:/directors";
    }

    @GetMapping("")
    public String getAll(Model model) {
        List<DirectorWithFilmsDTO> director = directorService.getAllDirectorsWithFilms();
        model.addAttribute("directors", director);
        return "directors/viewAllDirectors";
    }

    @GetMapping("/add")
    public String create() {
        return "directors/addDirector";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorDTO directorDTO) {
        directorService.create(directorDTO);
        return "redirect:/directors";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        directorService.delete(id);
        return "redirect:/directors";
    }


    @GetMapping("/add-film/{directorId}")
    public String addFilm(@PathVariable Long directorId,
                          Model model) {
        model.addAttribute("films", filmService.listAll());
        model.addAttribute("directorId", directorId);
        model.addAttribute("director", directorService.getOne(directorId).getDirectorsFIO());
        return "directors/addDirectorFilm";
    }

    @PostMapping("/add-film")
    public String addFilm(@ModelAttribute("directorFilmForm") AddFilmDTO addFilmDTO) {
        directorService.addFilms(addFilmDTO);
        return "redirect:/directors";
    }


}
