package ru.edu.filmlibrary.libraryproject.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Hidden
@Controller
public class MainController {
    @GetMapping("/")
    public String index() {
        return "index";
    }
}
