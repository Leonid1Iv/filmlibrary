package ru.edu.filmlibrary.libraryproject.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.edu.filmlibrary.libraryproject.dto.FilmDTO;
import ru.edu.filmlibrary.libraryproject.dto.FilmSearchDTO;
import ru.edu.filmlibrary.libraryproject.dto.FilmWithDirectorsDTO;
import ru.edu.filmlibrary.libraryproject.exception.MyDeleteException;
import ru.edu.filmlibrary.libraryproject.service.DirectorService;
import ru.edu.filmlibrary.libraryproject.service.FilmService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Hidden
@Controller
@RequestMapping("films")
@Slf4j
public class MVCFilmController {

    private final FilmService filmService;
    private final DirectorService directorService;

    public MVCFilmController(FilmService filmService, DirectorService directorService) {
        this.filmService = filmService;
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        Page<FilmWithDirectorsDTO> result = filmService.getAllFilmsWithDirectors(pageRequest);
        model.addAttribute("films", result);
        return "films/viewAllFilms";
    }


    //описание фильма
    // @MySecuredAnnotation(value = "ROLE_ADMIN")
    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("films", filmService.getFilmWithDirectors(id));
        return "films/viewFilm";
    }


    //Нарисует форму создания фильма
    @GetMapping("/add")
    public String create() {
        return "films/addFilm";
    }

    // Примет данные о создаваемом фильме и создаст в БД
    // Потом вернет нас на странуц со всеми фильмами
    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO filmDTO,
                         @RequestParam MultipartFile file) {
        if (file != null && file.getSize() > 0) {
            filmService.create(filmDTO, file);
        } else {
            filmService.create(filmDTO);
        }
        return "redirect:/films";
    }

    // обновление фильмов
    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("film", filmService.getOne(id));
        return "films/updateFilm";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("filmForm") FilmDTO filmDTO,
                         @RequestParam MultipartFile file) {
        if (file != null && file.getSize() > 0) {
            filmService.update(filmDTO, file);
        } else {
            filmService.update(filmDTO);
        }
        return "redirect:/films";
    }


    @PostMapping("/search")
    public String searchFilms(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("filmSearchForm") FilmSearchDTO filmSearchDTO,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));

        model.addAttribute("films", filmService.findFilms(filmSearchDTO, pageRequest));
        return "films/viewAllFilms";
    }

    // загрузка файла
    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody // когда идет запрос на сервис и не отрисовываются страницы
    public ResponseEntity<Resource> downloadFilm(@Param(value = "filmId") Long filmId) throws IOException {
        FilmDTO filmDTO = filmService.getOne(filmId);
        Path path = Paths.get(filmDTO.getOnlineCopyPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(this.headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        filmService.delete(id);
        return "redirect:/films";
    }

//    @ExceptionHandler(MyDeleteException.class)
//    public RedirectView handleError(HttpServletRequest req,
//                                    Exception ex,
//                                    RedirectAttributes redirectAttributes) {
//        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());
//        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
//        return new RedirectView("/films", true);
//    }


}
