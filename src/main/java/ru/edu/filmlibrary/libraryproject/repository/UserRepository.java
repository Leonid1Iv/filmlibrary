package ru.edu.filmlibrary.libraryproject.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.edu.filmlibrary.libraryproject.model.User;

@Repository
public interface UserRepository extends GenericRepository<User> {

    //select * from users where login = ?
    //@Query(nativeQuery = true, value = "select * from users where login = :login")
    User findUserByLogin(String login);

    User findUserByLoginAndIsDeletedFalse(String login);

    User findUserByEmail(String email);


    User findUserByChangePasswordToken(String token);

    @Query(nativeQuery = true,
            value = """
                    select u.*
                    from users u
                    where u.first_name ilike '%' || coalesce(:firstName, '%') || '%'
                    and u.last_name ilike '%' || coalesce(:lastName, '%') || '%'
                    and u.login ilike '%' || coalesce(:login, '%') || '%'
                     """)
    Page<User> searchUsers(String firstName,
                           String lastName,
                           String login,
                           Pageable pageable);

}
