package ru.edu.filmlibrary.libraryproject.repository;


import org.springframework.stereotype.Repository;
import ru.edu.filmlibrary.libraryproject.model.Order;

@Repository
public interface OrderRepository extends GenericRepository<Order>{
}
