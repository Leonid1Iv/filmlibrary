package ru.edu.filmlibrary.libraryproject.repository;

import org.springframework.stereotype.Repository;
import ru.edu.filmlibrary.libraryproject.model.Director;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {


}
