package ru.edu.filmlibrary.libraryproject.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.edu.filmlibrary.libraryproject.model.Film;


@Repository
public interface FilmRepository extends GenericRepository<Film> {
    @Query(nativeQuery = true,
            value = """       
                             select b.*
                             from films b
                             left join films_directors ba on b.id = ba.film_id
                             join directors a on a.id = ba.director_id
                             where b.title ilike '%' || :title || '%'
                             and cast(b.genre as char) like coalesce(:genre,'%')
                             and directors_fio ilike '%' || :fio || '%'
                    """)
    Page<Film> searchFilms(@Param(value = "genre") String genre,
                           @Param(value = "title") String title,
                           @Param(value = "fio") String fio,
                           Pageable pageable);

}
