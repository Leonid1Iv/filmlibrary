package ru.edu.filmlibrary.libraryproject.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserDTO extends GenericDTO {
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String birthDate;
    private String phone;
    private String address;
    private String email;
    private String changePasswordToken;
    private RoleDTO role;
    private LocalDate createdWhen;
    private Set<Long> orders;
    private boolean isDeleted;

}
