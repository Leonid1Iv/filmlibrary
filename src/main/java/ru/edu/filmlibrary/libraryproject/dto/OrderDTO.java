package ru.edu.filmlibrary.libraryproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO extends GenericDTO {
    private LocalDateTime rentDate;
    private Integer rentPeriod;
    private Boolean purchase;
    private Long userId;
    private Long filmId;

}
