package ru.edu.filmlibrary.libraryproject.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddFilmDTO extends GenericDTO{
    Long filmId;
    Long directorId;
}
