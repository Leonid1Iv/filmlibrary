package ru.edu.filmlibrary.libraryproject.dto;

import lombok.*;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DirectorDTO extends GenericDTO{
    private String directorsFIO;
    private String position;
    private Set<Long> filmsIds;
    private boolean isDeleted;

}
