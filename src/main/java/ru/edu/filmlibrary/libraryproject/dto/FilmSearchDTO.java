package ru.edu.filmlibrary.libraryproject.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.edu.filmlibrary.libraryproject.model.Genre;

@Getter
@Setter
@ToString
public class FilmSearchDTO {
    private String filmTitle;
    private String directorFio;
    private Genre genre;
}
