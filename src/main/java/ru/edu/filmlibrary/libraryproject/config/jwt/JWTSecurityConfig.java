
package ru.edu.filmlibrary.libraryproject.config.jwt;


import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import ru.edu.filmlibrary.libraryproject.service.userdetails.CustomUserDetailsService;

import java.util.Arrays;

import static ru.edu.filmlibrary.libraryproject.constants.SecurityConstants.RESOURCES_WHITE_LIST;
import static ru.edu.filmlibrary.libraryproject.constants.SecurityConstants.USERS_REST_WHITE_LIST;
import static ru.edu.filmlibrary.libraryproject.constants.UserRoleConstants.ADMIN;
import static ru.edu.filmlibrary.libraryproject.constants.UserRoleConstants.WORKER;
//
//// работает для REST
//@Configuration
//@EnableWebSecurity
//@EnableMethodSecurity
//public class JWTSecurityConfig {
//    private CustomUserDetailsService customUserDetailsService;
//    private final JWTTokenFilter jwtTokenFilter;
//
//    public JWTSecurityConfig(CustomUserDetailsService customUserDetailsService, JWTTokenFilter jwtTokenFilter) {
//        this.customUserDetailsService = customUserDetailsService;
//        this.jwtTokenFilter = jwtTokenFilter;
//    }
//
//    @Bean
//    public HttpFirewall httpFirewall() {
//        StrictHttpFirewall firewall = new StrictHttpFirewall();
////        firewall.setAllowUrlEncodedPercent(true);
////        firewall.setAllowUrlEncodedSlash(true);
////        firewall.setAllowSemicolon(true);
//        firewall.setAllowedHttpMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
//        return firewall;
//    }
//
//
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//        return http
//                //В Rest отключаем
//                .cors(AbstractHttpConfigurer::disable)
//                .csrf(AbstractHttpConfigurer::disable)
//
//                // Настройка http запросов - кому куда можно\нельзя
//                .authorizeHttpRequests(auth -> auth
//                        .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers(USERS_REST_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers("/directors/**").hasAnyRole( ADMIN, WORKER) // !!! определяем кто может получить доступ
//                        .anyRequest().authenticated())
//
//                .exceptionHandling().authenticationEntryPoint((request, response, authException) -> {
//                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());// если пытаемся получть доступ без авторизации
//                }).and()
//                // как будут вести себя сесси в рамках авторизации
//                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)) // у нас не будет доп HttpSession
//                //  JWT (проверка на валидность добавление фильтра) наличе JWT токена
//                .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class).userDetailsService(customUserDetailsService).build();
//    }
//
//    @Bean
//    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
//        return authenticationConfiguration.getAuthenticationManager();
//    }
//
//}


