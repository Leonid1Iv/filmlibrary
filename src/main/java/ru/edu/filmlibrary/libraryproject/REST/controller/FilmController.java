package ru.edu.filmlibrary.libraryproject.REST.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.filmlibrary.libraryproject.dto.FilmDTO;
import ru.edu.filmlibrary.libraryproject.model.Film;
import ru.edu.filmlibrary.libraryproject.service.FilmService;


@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы", description = "Контролллер для работы с фильмами фильмотеки")
public class FilmController extends GenericController<Film, FilmDTO> {
    protected final FilmService filmService;

    public FilmController(FilmService filmService) {
        super(filmService);
        this.filmService = filmService;
    }

//    @Operation(description = "Добавляем режиссёра к фильму", method = "addDirector")
//    @RequestMapping(value = "/addDirector",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<FilmDTO> addDirector(@RequestParam(value = "filmId") Long filmId,
//                                               @RequestParam(value = "directorId") Long directorId) {
//
//        return ResponseEntity.status(HttpStatus.OK).body(filmService.addDirector(filmId, directorId));
//    }

}
