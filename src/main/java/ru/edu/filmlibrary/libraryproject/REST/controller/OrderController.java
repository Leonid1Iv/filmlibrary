package ru.edu.filmlibrary.libraryproject.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.filmlibrary.libraryproject.dto.OrderDTO;
import ru.edu.filmlibrary.libraryproject.model.Order;
import ru.edu.filmlibrary.libraryproject.service.OrderService;


@RestController
@RequestMapping("/order")
@Tag(name = "Заказ", description = "Контроллер для работы с заказами фильмотеки")
public class OrderController extends GenericController<Order, OrderDTO> {
    protected final OrderService orderService;


    public OrderController(OrderService orderService) {
        super(orderService);
        this.orderService = orderService;
    }

    @Operation(description = "Взять фильм в аренду или купить", method = "addOrder")
    @RequestMapping(value = "/addOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> addOrder(@RequestBody OrderDTO newEntity) {
        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.orderFilm(newEntity));
    }
}
