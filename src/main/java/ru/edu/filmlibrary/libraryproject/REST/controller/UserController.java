package ru.edu.filmlibrary.libraryproject.REST.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.edu.filmlibrary.libraryproject.config.jwt.JWTTokenUtil;
import ru.edu.filmlibrary.libraryproject.dto.FilmDTO;
import ru.edu.filmlibrary.libraryproject.dto.LoginDTO;
import ru.edu.filmlibrary.libraryproject.dto.UserDTO;
import ru.edu.filmlibrary.libraryproject.model.User;
import ru.edu.filmlibrary.libraryproject.service.UserService;
import ru.edu.filmlibrary.libraryproject.service.userdetails.CustomUserDetailsService;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи", description = "Контроллер для работы с пользователям фильмотеки")
public class UserController extends GenericController<User, UserDTO>{


    private final CustomUserDetailsService customUserDetailsService;
    private final JWTTokenUtil jwtTokenUtil;
    private final UserService userService;

    public UserController(UserService userService,
                          CustomUserDetailsService customUserDetailsService,
                          JWTTokenUtil jwtTokenUtil) {
        super(userService);
        this.customUserDetailsService = customUserDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
    }

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
        Map<String, Object> response = new HashMap<>();
        log.info("LoginDTO: {}", loginDTO);
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
        log.info("foundUser, {}", foundUser);
        if (!userService.checkPassword(loginDTO.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\nНеверный пароль");
        }
        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("authorities", foundUser.getAuthorities());
        return ResponseEntity.ok().body(response);
    }





//
//    @Operation(description = "Показать все фильмы пользователя", method = "listFilm")
//    @RequestMapping(value = "/listFilm", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Set<FilmDTO>> listFilm(@RequestParam(value = "id") Long id) {
//        return ResponseEntity.status(HttpStatus.OK).body(userService.listFilm(id));
//    }




}

