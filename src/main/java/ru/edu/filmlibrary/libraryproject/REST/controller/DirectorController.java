package ru.edu.filmlibrary.libraryproject.REST.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.edu.filmlibrary.libraryproject.dto.DirectorDTO;
import ru.edu.filmlibrary.libraryproject.model.Director;
import ru.edu.filmlibrary.libraryproject.service.DirectorService;

@RestController
@RequestMapping("/directors")
@Tag(name = "Директора", description = "Контролллер для работы с директорами фильмотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController extends GenericController<Director, DirectorDTO> {
    private final DirectorService directorService;


    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;
    }

//        @Secured(value = "ROLE_ADMIN")  - можно использовать для разграничение ролей
//        public void test() {
//
//        }

//    @Operation(description = "Добавляем фильм к режиссеру", method = "addFilm")
//    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "filmId") Long filmId,
//                                               @RequestParam(value = "directorId") Long directorId) {
//        return ResponseEntity.status(HttpStatus.OK).body(directorService.addFilm(directorId, filmId));
//    }


    @Operation(description = "Добавляем фильм к режиссеру", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm(@RequestParam(value = "filmId") Long filmId,
                                               @RequestParam(value = "directorId") Long directorId) {
        return ResponseEntity.status(HttpStatus.OK).body(directorService.addFilm(directorId, filmId));
    }




}
