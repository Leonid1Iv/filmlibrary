package ru.edu.filmlibrary.libraryproject.constants;

public interface UserRoleConstants {

    String ADMIN = "ADMIN";
    String WORKER = "WORKER";
    String USER = "USER";


}
