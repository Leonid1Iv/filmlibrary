package ru.edu.filmlibrary.libraryproject.constants;

public interface FileDirectoriesConstants {
    String BOOKS_UPLOAD_DIRECTORY = "files/films";
}
