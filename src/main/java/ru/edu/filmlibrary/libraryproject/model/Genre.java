package ru.edu.filmlibrary.libraryproject.model;

public enum Genre {
    FANTASY("Фантастика"),
    SCIENCE_FUNCTION("Научная фантастика"),
    DRAMA("Драма"),
    NOVEL("Роман");

    private final String genreTextDisplay;

    Genre(String genreName) {
        this.genreTextDisplay = genreName;
    }

    public String getGenreTextDisplay() {
        return this.genreTextDisplay;
    }
}
