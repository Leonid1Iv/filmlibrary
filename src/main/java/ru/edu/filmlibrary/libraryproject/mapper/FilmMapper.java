package ru.edu.filmlibrary.libraryproject.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.edu.filmlibrary.libraryproject.dto.FilmDTO;
import ru.edu.filmlibrary.libraryproject.model.Film;
import ru.edu.filmlibrary.libraryproject.model.GenericModel;
import ru.edu.filmlibrary.libraryproject.repository.DirectorRepository;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDTO> {
    private final DirectorRepository directorRepository;

    private final ModelMapper modelMapper;

    protected FilmMapper(ModelMapper modelMapper, DirectorRepository directorRepository) {
        super(modelMapper, Film.class, FilmDTO.class);
        this.modelMapper = modelMapper;
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter());
//                .addMappings(m -> m.skip(Film::setPremierYear)).setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFields(FilmDTO source, Film destination) {
        if (!Objects.isNull(source.getDirectorsIds()) && source.getDirectorsIds().size() > 0) {
            destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getDirectorsIds())));
        } else {
            destination.setDirectors(Collections.emptySet());
        }
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy");
//        LocalDate date = LocalDate.parse(source.getPremierYear(), formatter);
//        destination.setPremierYear(date);


    }

    @Override
    void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setDirectorsIds(getIds(source));
    }

    protected Set<Long> getIds(Film film) {
        return Objects.isNull(film) || Objects.isNull(film.getDirectors())
                ? null
                : film.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
