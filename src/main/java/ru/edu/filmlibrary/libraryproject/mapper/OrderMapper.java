package ru.edu.filmlibrary.libraryproject.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.edu.filmlibrary.libraryproject.dto.OrderDTO;
import ru.edu.filmlibrary.libraryproject.model.Order;
import ru.edu.filmlibrary.libraryproject.repository.FilmRepository;
import ru.edu.filmlibrary.libraryproject.repository.UserRepository;

import java.util.Set;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDTO> {

    private FilmRepository filmRepository;
    private UserRepository userRepository;

    protected OrderMapper(FilmRepository filmRepository, UserRepository userRepository, ModelMapper modelMapper) {
        super(modelMapper, Order.class, OrderDTO.class);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setFilmId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrderDTO::setUserId)).setPostConverter(toDTOConverter());

        super.modelMapper.createTypeMap(OrderDTO.class, Order.class)
                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow(() -> new NotFoundException("Фильм не наден")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователь не наден")));


    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setFilmId(source.getFilm().getId());
    }

    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}
