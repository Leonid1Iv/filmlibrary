package ru.edu.filmlibrary.libraryproject.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.edu.filmlibrary.libraryproject.dto.DirectorDTO;
import ru.edu.filmlibrary.libraryproject.model.Director;
import ru.edu.filmlibrary.libraryproject.model.GenericModel;
import ru.edu.filmlibrary.libraryproject.repository.FilmRepository;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorMapper extends GenericMapper<Director, DirectorDTO> {
    private final ModelMapper modelMapper;
    private final FilmRepository filmRepository;

    protected DirectorMapper(ModelMapper modelMapper, FilmRepository filmRepository) {
        super(modelMapper, Director.class, DirectorDTO.class);
        this.modelMapper = modelMapper;
        this.filmRepository = filmRepository;

    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Director.class, DirectorDTO.class)
                .addMappings(m -> m.skip(DirectorDTO::setFilmsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DirectorDTO.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilms)).setPostConverter(toEntityConverter());
    }

    @Override
    void mapSpecificFields(DirectorDTO source, Director destination) {
        if (!Objects.isNull(source.getFilmsIds()) && source.getFilmsIds().size() > 0) {
            destination.setFilms(new HashSet<>(filmRepository.findAllById(source.getFilmsIds())));
        } else {
            destination.setFilms(Collections.emptySet());
        }
    }

    @Override
    void mapSpecificFields(Director source, DirectorDTO destination) {
        destination.setFilmsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Director director) {
        return Objects.isNull(director) || Objects.isNull(director.getFilms())
                ? null
                : director.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
