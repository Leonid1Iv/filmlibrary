package ru.edu.filmlibrary.libraryproject.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.edu.filmlibrary.libraryproject.dto.UserDTO;
import ru.edu.filmlibrary.libraryproject.model.GenericModel;
import ru.edu.filmlibrary.libraryproject.model.User;
import ru.edu.filmlibrary.libraryproject.repository.OrderRepository;
import ru.edu.filmlibrary.libraryproject.utils.DateFormatter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {

    private OrderRepository orderRepository;

    protected UserMapper(ModelMapper modelMapper, OrderRepository orderRepository) {
        super(modelMapper, User.class, UserDTO.class);
        this.orderRepository = orderRepository;
    }


    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setOrders)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setOrders)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setBirthDate)).setPostConverter(toEntityConverter());
    }


    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        if (!Objects.isNull(source.getOrders())) {
            destination.setOrders(new HashSet<>(orderRepository.findAllById(source.getOrders())));
        } else {
            destination.setOrders(Collections.emptySet());
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        destination.setOrders(getIds(source));
    }


    protected Set<Long> getIds(User user) {
        return Objects.isNull(user) || Objects.isNull(user.getOrders())
                ? null
                : user.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
