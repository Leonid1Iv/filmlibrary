package ru.edu.filmlibrary.libraryproject.mapper;

import ru.edu.filmlibrary.libraryproject.dto.GenericDTO;
import ru.edu.filmlibrary.libraryproject.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDTO> {

    E toEntity(D dto); // преобразование dto в entity
    D toDTO(E entity);
    List<E> toEntities(List<D> dtoList);
    List<D> toDTOs(List<E> entityList);

}
