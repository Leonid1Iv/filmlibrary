package ru.edu.filmlibrary.libraryproject.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.filmlibrary.libraryproject.dto.*;
import ru.edu.filmlibrary.libraryproject.mapper.DirectorMapper;
import ru.edu.filmlibrary.libraryproject.mapper.DirectorWithFilmsMapper;
import ru.edu.filmlibrary.libraryproject.model.Director;
import ru.edu.filmlibrary.libraryproject.model.Film;
import ru.edu.filmlibrary.libraryproject.repository.DirectorRepository;
import ru.edu.filmlibrary.libraryproject.repository.FilmRepository;

import java.util.List;

@Service
@Slf4j
public class DirectorService extends GenericService<Director, DirectorDTO> {
    private final DirectorRepository directorRepository;
    private final FilmRepository filmRepository;
    private final DirectorWithFilmsMapper directorWithFilmsMapper;


    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper directorMapper,
                              FilmRepository filmRepository,
                              DirectorWithFilmsMapper directorWithFilmsMapper) {
        super(directorRepository, directorMapper);
        this.directorRepository = directorRepository;
        this.filmRepository = filmRepository;
        this.directorWithFilmsMapper = directorWithFilmsMapper;

    }


    public DirectorDTO addFilm(Long directorId, Long filmId) {
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссер с таким Id не найден"));
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм с таким Id не найден"));
        director.getFilms().add(film);
        return mapper.toDTO(directorRepository.save(director));
    }

//    //описание фильма
//    public DirectorWithFilmsDTO getDirectorWithFilms(Long id){
//        return directorWithFilmsMapper.toDTO(mapper.toEntity(super.getOne(id)));
//    }


    // обновить директора

//    @Override
//    public DirectorDTO update( final DirectorDTO updateDirector){
//        return mapper.toDTO(repository.save(mapper.toEntity(updateDirector)));
//    }


    public List<DirectorWithFilmsDTO> getAllDirectorsWithFilms() {
        return directorWithFilmsMapper.toDTOs(directorRepository.findAll());
    }


    @Override
    public void delete(Long id) {
        Director director = directorRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Фильма с заданным ID=" + id + " не существует"));
        directorRepository.deleteById(director.getId());
    }

    public void addFilms(AddFilmDTO addFilmDTO) {
        DirectorDTO director = getOne(addFilmDTO.getDirectorId());
        director.getFilmsIds().add(addFilmDTO.getFilmId());
        update(director);

    }

}




