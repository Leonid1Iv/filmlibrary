package ru.edu.filmlibrary.libraryproject.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.edu.filmlibrary.libraryproject.dto.OrderDTO;
import ru.edu.filmlibrary.libraryproject.mapper.OrderMapper;
import ru.edu.filmlibrary.libraryproject.model.Film;
import ru.edu.filmlibrary.libraryproject.model.Order;
import ru.edu.filmlibrary.libraryproject.model.User;
import ru.edu.filmlibrary.libraryproject.repository.FilmRepository;
import ru.edu.filmlibrary.libraryproject.repository.OrderRepository;
import ru.edu.filmlibrary.libraryproject.repository.UserRepository;

import java.time.LocalDate;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {
    protected final OrderRepository orderRepository;
    protected final UserRepository userRepository;
    protected final FilmRepository filmRepository;

    public OrderService(OrderRepository orderRepository, OrderMapper orderMapper,
                        UserRepository userRepository, FilmRepository filmRepository) {
        super(orderRepository, orderMapper);
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
    }

    public OrderDTO orderFilm(OrderDTO orderDTO) {
        Film film = filmRepository.findById(orderDTO.getFilmId())
                .orElseThrow(() -> new NotFoundException("Фильм с таким Id не найден"));
        User user = userRepository.findById(orderDTO.getUserId())
                .orElseThrow(() -> new NotFoundException("Пользователь с таким Id не найден"));
        Order order = new Order();
        order.setUser(user);
        order.setFilm(film);
        if (orderDTO.getPurchase()) {
            order.setPurchase(true);
        } else {
            order.setPurchase(false);
            order.setRentDate(LocalDate.now().atStartOfDay());
            order.setRentPeriod(orderDTO.getRentPeriod());
        }
        orderRepository.save(order);
        return mapper.toDTO(order);

    }
}
