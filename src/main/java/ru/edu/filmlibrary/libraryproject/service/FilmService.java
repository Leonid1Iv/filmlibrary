package ru.edu.filmlibrary.libraryproject.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;
import ru.edu.filmlibrary.libraryproject.dto.FilmDTO;
import ru.edu.filmlibrary.libraryproject.dto.FilmSearchDTO;
import ru.edu.filmlibrary.libraryproject.dto.FilmWithDirectorsDTO;
import ru.edu.filmlibrary.libraryproject.exception.MyDeleteException;
import ru.edu.filmlibrary.libraryproject.mapper.FilmMapper;
import ru.edu.filmlibrary.libraryproject.mapper.FilmWithDirectorsMapper;
import ru.edu.filmlibrary.libraryproject.model.Film;
import ru.edu.filmlibrary.libraryproject.repository.FilmRepository;
import ru.edu.filmlibrary.libraryproject.utils.FileHelper;

import java.util.List;

@Service
@Slf4j
public class FilmService extends GenericService<Film, FilmDTO> {

    private FilmRepository filmRepository;


    private final FilmWithDirectorsMapper filmWithDirectorsMapper;


    protected FilmService(FilmRepository filmRepository,
                          FilmMapper filmMapper,
                          FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(filmRepository, filmMapper);
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
        this.filmRepository = filmRepository;
    }


    //добавление файла
    public FilmDTO create(final FilmDTO object, MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        return mapper.toDTO(filmRepository.save(mapper.toEntity(object)));
    }

    public FilmDTO update(final FilmDTO object, MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        return mapper.toDTO(filmRepository.save(mapper.toEntity(object)));
    }


    //пагинация
    public Page<FilmWithDirectorsDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmPaginates = filmRepository.findAll(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmPaginates.getContent());
        return new PageImpl<>(result, pageable, filmPaginates.getTotalElements());
    }


    //описание фильма
    public FilmWithDirectorsDTO getFilmWithDirectors(Long id) {
        return filmWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }


    //поиск фильма
    public Page<FilmWithDirectorsDTO> findFilms(FilmSearchDTO filmSearchDTO, Pageable pageable) {
        String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
        Page<Film> filmPaginates = filmRepository.searchFilms(genre, filmSearchDTO.getFilmTitle(), filmSearchDTO.getDirectorFio(), pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmPaginates.getContent());
        return new PageImpl<>(result, pageable, filmPaginates.getTotalElements());

    }

    //удаление фильма
    @Override
    public void delete(Long id) throws MyDeleteException {
        Film film = filmRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Фильма с заданным ID=" + id + " не существует"));
        if (film.getOnlineCopyPath() != null && !film.getOnlineCopyPath().isEmpty()) {
            FileHelper.deleteFile(film.getOnlineCopyPath());
        }
        filmRepository.deleteById(film.getId());
    }

}




