package ru.edu.filmlibrary.libraryproject.service;

import ru.edu.filmlibrary.libraryproject.dto.DirectorDTO;
import ru.edu.filmlibrary.libraryproject.model.Director;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface DirectorTestData {

    DirectorDTO DIRECTOR_DTO_1 = new DirectorDTO("directorFio1"
                                                  , "режиссер"
                                                  , new HashSet<>()
                                                  , false);

    DirectorDTO DIRECTOR_DTO_2 = new DirectorDTO("directorFio2"
                                                          , "position2"
                                                          , new HashSet<>()
                                                          , false);

    DirectorDTO DIRECTOR_DTO_3_DELETE = new DirectorDTO("directorFio3"
                                                          , "position3"
                                                          , new HashSet<>()
                                                          , true);


    List<DirectorDTO> DIRECTOR_DTO_LIST = Arrays.asList(DIRECTOR_DTO_1, DIRECTOR_DTO_2, DIRECTOR_DTO_3_DELETE);


    Director DIRECTOR_1 = new Director("director1", "position1", null);
    Director DIRECTOR_2 = new Director("director2", "position2", null);
    Director DIRECTOR_3 = new Director("director3", "position3", null);



    List<Director> DIRECTOR_LIST = Arrays.asList(DIRECTOR_1, DIRECTOR_2 ,DIRECTOR_3);
}
