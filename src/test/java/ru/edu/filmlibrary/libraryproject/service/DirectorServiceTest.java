package ru.edu.filmlibrary.libraryproject.service;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import ru.edu.filmlibrary.libraryproject.dto.DirectorDTO;
import ru.edu.filmlibrary.libraryproject.mapper.DirectorMapper;
import ru.edu.filmlibrary.libraryproject.mapper.DirectorWithFilmsMapper;
import ru.edu.filmlibrary.libraryproject.model.Director;
import ru.edu.filmlibrary.libraryproject.repository.DirectorRepository;
import ru.edu.filmlibrary.libraryproject.repository.FilmRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class DirectorServiceTest extends GenericTest<Director, DirectorDTO> {
    private final   DirectorRepository directorRepository = Mockito.mock(DirectorRepository.class);
    private final DirectorMapper directorMapper = Mockito.mock(DirectorMapper.class);


    public DirectorServiceTest() {
        super();
        FilmRepository filmRepository = Mockito.mock(FilmRepository.class);
        DirectorWithFilmsMapper directorWithFilmsMapper = Mockito.mock(DirectorWithFilmsMapper.class);
        service = new DirectorService(directorRepository, directorMapper, filmRepository, directorWithFilmsMapper);
    }


    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(directorRepository.findAll()).thenReturn(DirectorTestData.DIRECTOR_LIST);
        Mockito.when(directorMapper.toDTOs(DirectorTestData.DIRECTOR_LIST)).thenReturn(DirectorTestData.DIRECTOR_DTO_LIST);
        List<DirectorDTO> directorDTOS = service.listAll();
        log.info("Testing getAll(): " + directorDTOS);
        assertEquals(DirectorTestData.DIRECTOR_LIST.size(), directorDTOS.size());


    }

    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(directorMapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(directorMapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(directorRepository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO directorDTO = service.create(DirectorTestData.DIRECTOR_DTO_1);
        log.info("Testing create(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);

    }

    @Order(3)
    @Test
    @Override
    protected void create() {
        Mockito.when(directorMapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(directorMapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(directorRepository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO directorDTO = service.create(DirectorTestData.DIRECTOR_DTO_1);
        log.info("Testing create(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);


    }
    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(directorMapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(directorMapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(directorRepository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO directorDTO = service.update(DirectorTestData.DIRECTOR_DTO_1);
        log.info("Testing update(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);

    }

    @Order(4)
    @Test
    @Override
    protected void delete() {

    }



    @Order(6)
    @Test
    @Override
    protected void restore() {

    }


    @Order(7)
    @Test
    void searchDirectors() {

    }

    @Order(8)
    @Test
    void addFilm() {


    }

    @Order(9)
    @Test
    protected void getAllNotDeleted() {

    }
}
